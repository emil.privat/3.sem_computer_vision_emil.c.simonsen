#pragma once
#include "opencv2/imgproc.hpp"
#include "opencv2/highgui.hpp"
#include <iostream>

using namespace cv;

Mat TackBarCannyThreshold(Mat src)
{
    Mat dst, detected_edges;
    int lowThreshold = 0;
    const int max_lowThreshold = 100;
    const int ratio = 3;
    const int kernel_size = 3;
    const char* window_name = "Edge Map";

    namedWindow(window_name, WINDOW_NORMAL);
    createTrackbar("LowThreshold", window_name, &lowThreshold, max_lowThreshold);
    GaussianBlur(src, detected_edges, Size(3, 3), 0);
    Canny(detected_edges, detected_edges, lowThreshold, lowThreshold * ratio, kernel_size);
    dst = Scalar::all(0);
    src.copyTo(dst, detected_edges);
    imshow(window_name, dst);
    waitKey(1);
    return dst;
}
