#pragma once
#include "opencv2/imgproc.hpp"
#include "opencv2/highgui.hpp"
#include <iostream>
#include <filters.h>
#include <detectors.h>

//prepping an immage for extracting contrours
Mat prep(Mat mysrc) {

    Mat image, img, DoGimg, mydst, dilated;
    Mat kernel = getStructuringElement(MORPH_RECT, Size(11, 11));

    DoGimg = DoG(mysrc);
    cvtColor(DoGimg, image, COLOR_BGR2GRAY);
    Canny(image, mydst, 10, 50);
    dilate(mydst, dilated, kernel);
    return dilated;
}