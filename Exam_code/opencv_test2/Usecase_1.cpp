#include <opencv2/core/core.hpp>
#include <opencv2/highgui/highgui.hpp>
#include <opencv2/imgproc.hpp>
#include <iostream>
#include <stdint.h>
#include <detectors.h>
#include <thresholding.h>
#include <filters.h>
#include <myheaders.h>



using namespace cv;
using namespace std; //includes std so you don't have to type std

Mat image, image2, image3, src, src2, src3, channel[3], DoGimg, postimg, cont1, cont2, img_gray, edges, dilated, drawn_img1, drclone;
Mat kernel = getStructuringElement(MORPH_RECT, Size(3, 3));
string file = "Drone_greenscreen_25.jpg", file2 = "Drone_blue_1.jpg", o = "o.jpg", file3 = "drones_1.jpg", klip = "Drone_klip.mp4";



int main(int argc, char** argv)
{
	//--------------------------------------------------------------------------------------------------------------------------------------------------------
	/*
	//USECASE 1
	src = imread("C:/Users/Emil-PC/Desktop/It_Teknologi/computervision/Portfolio/Case_1/Image_samples/drone/"+(file)); //insert "IMREAD_GRAYSCALE" for grayscale
	src2 = imread("C:/Users/Emil-PC/Desktop/It_Teknologi/computervision/Portfolio/Case_1/Image_samples/drone/" + (o)); //insert "IMREAD_GRAYSCALE" for grayscale
	
	
	vector<vector<Point>> cont_templ;
	vector<vector<Point>> contours;
	vector<Vec4i> hierarchy;


	resize(src, image, Size(825, 550));
	resize(src2, image2, Size(825, 550));
	
	cont_templ = extractContours(prep(image)); //finding the contours on an image that has been filtered with DoG and dilated
	contours = extractContours(prep(image2));//finding the contours on an image that has been filtered with DoG and dilated

	Mat copy = image.clone();
	Mat copy2 = image2.clone();
	drawContours(image, cont_templ, -1, Scalar(255, 0, 255), 2);
	drawContours(image2, contours, -1, Scalar(255, 0, 255), 2);
	imshow("contoured", (image));
	imshow("contoured2", (image2));

	matchContoursSimple(cont_templ, contours);

	drawContours(copy, contour_template_match, -1, Scalar(255, 0, 255), 2);
	drawContours(copy2, array_of_contours_match, -1, Scalar(255, 0, 255), 2);
	imshow("matches",(copy));
	imshow("matches2",(copy2));
	
	waitKey(0);
	*/

	//------------------------------------------------------------------------------------------------------------------------------------------------------

	/*
	//USECASE2
	RNG rng(12345);
	src3 = imread("C:/Users/Emil-PC/Desktop/It_Teknologi/computervision/Portfolio/Case_1/Image_samples/drone/" + (file)); //insert "IMREAD_GRAYSCALE" for grayscale
	resize(src3, image3, Size(825, 550));
	Mat img = prep(image3).clone();

	vector<vector<Point>> contours;
	vector<Vec4i> hierarchy2;

	findContours(img, contours, RETR_EXTERNAL, CHAIN_APPROX_SIMPLE);
	
	vector<vector<Point> > contours_poly(contours.size());
	vector<Rect> boundRect(contours.size());
	vector<Point2f>centers(contours.size());
	vector<float>radius(contours.size());
	for (size_t i = 0; i < contours.size(); i++)
	{
		approxPolyDP(contours[i], contours_poly[i], 2, true);
		boundRect[i] = boundingRect(contours_poly[i]);
	}

	Mat drawing = Mat::zeros(img.size(), CV_8UC3);
	
	for (size_t i = 0; i < contours.size(); i++)
	{
		Scalar color = Scalar(rng.uniform(0, 256), rng.uniform(0, 256), rng.uniform(0, 256));
		drawContours(drawing, contours_poly, (int)i, color);
		rectangle(drawing, boundRect[i].tl(), boundRect[i].br(), color, 2);
	}
	imshow("preped", img);
	imshow("detect", drawing);
	imshow("original", image3);
	waitKey(0);
	*/

	//----------------------------------------------------------------------------------------------------------------------------------------------------------

	//USECASE3
	VideoCapture video1("C:/Users/Emil-PC/Desktop/It_Teknologi/computervision/Portfolio/Case_1/Image_samples/drone/" + (klip));
	RNG rng(12345);
	vector<vector<Point>> contours;

	int frame_width = video1.get(cv::CAP_PROP_FRAME_WIDTH);
	int frame_height = video1.get(cv::CAP_PROP_FRAME_HEIGHT);

	VideoWriter video("dialted.mp4", cv::VideoWriter::fourcc('M', 'P', '4', 'V'), 20, Size(550, 366));
	while (1) {

		// Defining veriabels
		Mat frame, prepframe;
		Mat img,dilatedimg;

		// Capture frame-by-frame
		video1 >> frame;


		// If the frame is empty, break immediately
		if (frame.empty())
			break;

		// proccessing frames
		resize(frame, img, Size(550, 366));
		frame = prep(img).clone();
		Mat og = img.clone();
		findContours(frame, contours, RETR_EXTERNAL, CHAIN_APPROX_SIMPLE);

		vector<vector<Point> > contours_poly(contours.size());
		vector<Rect> boundRect(contours.size());
		vector<Point2f>centers(contours.size());
		vector<float>radius(contours.size());
		for (size_t i = 0; i < contours.size(); i++)
		{
			approxPolyDP(contours[i], contours_poly[i], 2, true);
			boundRect[i] = boundingRect(contours_poly[i]);
		}

		Mat drawing = Mat::zeros(img.size(), CV_8UC3);

		for (size_t i = 0; i < contours.size(); i++)
		{
			Scalar color = Scalar(0,0,255);
			drawContours(drawing, contours_poly, (int)i, color);
			drclone = drawing.clone();
			rectangle(img, boundRect[i].tl(), boundRect[i].br(), color, 2);
			rectangle(drawing, boundRect[i].tl(), boundRect[i].br(), color, 2);
		}

		// writeing file
		video.write(img);
		// Display the resulting frame
		imshow("og", og);
		imshow("dilated", frame);
		imshow("drawing", drclone);
		imshow("detecting", drawing);

		// Press  ESC on keyboard to exit
		char c = (char)waitKey(25);
		if (c == 27)
			break;
	}

	// When everything done, release the video capture object
	video.release();
	cout << endl;
	cout << "released" << endl;
}